# plagueOS

*"But what does it mean? The plague? It's life, that's all." - A. Camus*


# Install Guide

## Prerequisites
1. Internet Connection
2. UEFI
3. Recommended minimum 8GB of RAM, 50GB of storage

## Flash USB
- Download Void musl live image from `https://voidlinux.org/download/`
	- Validate with PGP key or the SHA256 sum listed with the ISO files
	- Flash the .iso file to a USB Drive
		- For Windows, download `Rufus` or `Balena Etcher` to flash the .iso
		- For Linux, run the following dd command:
			- Run  `sudo fdisk -l` to show the flashdrive
			- Run `sudo dd if=void*.iso of=/dev/<USB>`
				- *example:* `/dev/sdb`
			- After the flash has been completed, run `umount /dev/<USB>` to unmount the drive

## Base Install & Hardening
 - Plug in the flashdrive and boot from the USB (typically, pressing ESC/F8,F9,F12 allows you to select a boot device upon system startup)
 - Start a live session in void
    - Sign in with credentials: `root:voidlinux`
    - `xbps-install -S xbps git netcat`
    - `git clone -b master --single-branch https://git.envs.net/whichdoc/plagueos.git`
    - `bash plagueos/plague-install`
    - The ISO is now released in the following [git](https://git.envs.net/whichdoc/plagueos_iso). Note: The ISO may not have the most recent changes that are available in the master branch

### Install Options
 - During the install, multiple configurations will be displayed. The selections are between the following:
	- `Cli-only` (Minimal installation for advanced users)
	- `Sway WM` (Minimalist Window Manager (WM) for advanced users)
    - `KDE` (Highly customizable Desktop Environment that is user friendly and moderately lightweight. Recommended to both novice and advanced to users.)
	- `Gnome w/ Wayland` (For novice users who desire gutted desktop environment)
		- Note: `Gnome w/ Wayland` has an active bug with `GDM` that will disrupt sessions upon lock/hibernate/suspend actions. To get back in your session following a lock, press `CTRL+ALT+F7.`
	- `Gnome w/ LightDM` (Heaviest but relatively minimalist option. This is a workaround while the `GDM` is being resolved.)
		- Note: You will need to set up your own keyboard shortcuts with LightDM. From `gnome-control-center`, you can add custom shortcuts such as screenlock by entering the command `dm-tool lock` into the shortcut prompt
        
## Implementation Goals
- [X] Full System Build
- [X] Dependency Install
- [X] Hardened Memory Allocator system-wide LD_PRELOAD
- [ ] Hardened Kernel w/ PaX, tirdad patch, grsec patchsets, and gutted modules (WIP)
- [X] Blacklisted Kernel Modules
- [X] Blacklisted File Systems
- [X] Blacklisted Network Protocols
- [X] IPTables Packet Filtering
- [X] Custom LUKS Encryption (AES256XTS+Argon2id KDF)
- [X] Hardened GRUB Boot Parameters
- [X] Hide Process IDs
- [X] UMASK 0027 to be the default system-wide
- [X] Locked `root` account, `admin` account for elevated privileges
- [X] Secure fstab configuration (Bind for var and tmp)
- [X] Rolled in Whonix's hide-hardware-info script (See [here](https://codeberg.org/SalamanderSecurity/PARSEC/src/branch/main/parsec/bin/hide-hardware-info))
- [X] Increased Entropy with use of `haveged` and `jitterentropy`
- [X] bwrap profiling for building VMs
- [X] Increased password hashing rounds
- [X] Full Wayland-only options
- [X] Selection between WM, DE, or CLI-only
- [X] Use of doas over sudo
- [X] Generic Machine ID
- [X] Randomized MAC address for NIC
- [X] Memory erasure/poisoning
- [X] USBGuard Implementation
- [X] Import & Verification of Kicksecure & Whonix
- [X] Hardened SSH configuration (SSH not installed on host by default)
- [X] Encrypted DNS with DNSCrypt by default
- [X] Directory permission hardening
- [ ] Feature: Grub password (optional)
- [ ] Feature: Implement OS component updates by setting up update infrastructure and by adding a custom repository
- [ ] Feature: Implement WiFi Interface softblocking (psec-rfkill)
- [ ] Feature: bwrap sandbox of all the executables used by `user`
- [ ] Feature: Nuke LUKS keys by using a special passphrase (See reference [here](https://gitlab.com/kalilinux/packages/cryptsetup-nuke-keys))
- [ ] Feature: Add VM artifacts to the main host to trick malware (WIP)
- [ ] Feature: Self-Signed Verified Boot
- [X] All commits to contain PGP signatures
- [ ] Audit of the Installation Script (Ongoing)

## Trust Model 
It is important to operate on a zero trust model. Since this is an impossible feat, it should at least be known where trust is given and impose restrictions/limitations where feasible. 
- Hardware
    - CPU
    - Miscellaneous Integrated Chipsets
- Software
    - Hypervisor ([Void OS](https://voidlinux.org/))
        - Void Developers
    - PlagueOS's Script Build
    - [SwayWM](https://swaywm.org) - Option during the installation
    - [KDE](https://kde.org/) - Option during the installation
    - [GNOME Core](https://www.gnome.org/) - Option during the installation
		- Default GDM (Wayland only) - Option 1
		- LightDM (Wayland + Xorg) - Option 2
    - Graphene's [Hardened Malloc](https://github.com/GrapheneOS/hardened_malloc) 
    - Guest OS of Choice (varying levels of security/restrictions controlled by the VM's user)
		- [KickSecure](https://www.whonix.org/wiki/Kicksecure/KVM)
        - [Whonix](https://www.whonix.org/wiki/KVM)
        - [ParrotOS](https://parrotsec.org/)
        - [Void](https://voidlinux.org/)
        - [Arch](https://archlinux.org/)
        - Any other OS desired by the user

## Schema

    --> /dev/sda
        --> /dev/sda1   EFI Partition   /boot
        --> /dev/sda2   Linux LVM
            --> Crypt  Luks2 encrypted drive
                --> LogicalVolume_Home      Home Partition      /home
                --> LogicalVolume_Root      Root Partition      /
                --> LogicalVolume_Var       Var Partition       /var
                --> LogicalVolume_Swap      Swap Partition      [Swap]


## Usage
This OS is designed to run as a hypervisor and launch Guest virtual machines (VMs); The OS is designed to be a minimal build and have a restricted userspace. The guests are launched from `virt-manager`.
- Run Virt-Manager either by opening a terminal and running the command `virt-manager` or by opening the application via its GUI.
- During the installation, there is an option to pull down and import [KickSecure](https://www.whonix.org/wiki/Kicksecure/KVM) which is the clearnet baseline of Whonix for day-to-day activities.
- If the torrified version of Kicksecure - [Whonix](https://www.whonix.org/wiki/KVM) - is chosen, you must boot both the `Whonix-Gateway` and `Whonix-Workstation` VMs with `virt-manager`.
	- Note: If the `Whonix-Gateway` is down, the `Whonix-Workstation` will have no internet access.


Feel free to contact us in the PlagueOS matrix [chat](https://matrix.to/#/!BAkNDIUQRnHPJwkwsq:matrix.org?via=matrix.org)

## Creating the installation of PlagueOS on the Void .iso
 - Requirement: [void-mklive](https://github.com/void-linux/void-mklive.git) repository.
 - `./mklive.sh -a x86_64-musl -o plagueOS.iso -p "base-devel xz curl gnupg2 tor cryptsetup lvm2" -T "plagueOS" -v linux5.10 -I includedir -C "nomodeset"`
 	- Note: You have to create a directory 'includedir' in the 'void-mklive' repo and add the `PlagueOS files` inside it.