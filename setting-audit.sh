#!/bin/bash
# Execute script with doas for validation of doas config over sudo

echo "OS hardening audit"
echo " "

echo "Generic host name: "
cat /etc/hosts
echo " "

echo "Randomized Mac"
ip a | grep "permaddr"

echo "Presence of hardened_malloc: "
ls /usr/local/lib/hardened_malloc.so
echo " "

echo "List sysctl configuration files: "
ls -la /etc/sysctl.d/

echo "Display sysctl configuration: "
cat /etc/sysctl.d/10*
echo " "
cat /etc/sysctl.d/20*
echo " "
cat /etc/sysctl.d/30*
echo " "
cat /etc/sysctl.d/40*
echo " "
cat /etc/sysctl.d/50*
echo " "
cat /etc/sysctl.d/60*
echo " "
cat /etc/sysctl.d/70*

echo " "
echo " "
echo " "

echo "/etc/modprobe.d/ configuration: "
echo " "
cat /etc/modprobe.d/blacklist.conf 
echo " "
echo " "
echo " "

echo "IPTables set?"
iptables -L -n
echo " "
echo " "
echo " "

echo "Root and admin user status: "
passwd -S root
passwd -S admin
echo " "
echo " "
echo " "

echo "Present /plague/bin files"
ls -la /plague/bin/
echo " "
echo " "
echo " "

echo "Core dumps disabled?"
cat /etc/security/limits.conf | grep "hard core"
echo " "
echo " "
echo " "

echo "Validate UMASK is set"
cat /etc/profile | grep "umask 0"
echo " "
echo " "
echo " "

echo "Check machine ID (should be generic 'b08dfa6083e7567a1921a715000001fb')"
cat /etc/machine-id 
echo " "
echo " "
echo " "

echo "user enumeration: "
cat /etc/passwd
echo " "
echo " "
echo " "
 
echo "/etc/fstab configuration: "
cat /etc/fstab
echo " "
echo " "
echo " "
echo "hardened boot parameters: "
cat /etc/default/grub | grep CMDLINE_LINUX_DEFAULT=
echo " "
echo " "
echo " "

echo "Check directory permissions"
ls -ld /boot
ls -ld /etc/iptables
ls -ld /usr/src
ls -ld /lib/modules
ls -ld /usr/lib/modules
ls -ld /home/admin
echo " "
echo " "
echo " "
echo "Audit complete"
exit
