#!/bin/bash

function helpMenu {

  echo "-i --iso    *Required* Path to the ISO you want to install"
  echo "-n --name   *Required* Name of the VM you are creating"
  echo "-r --ram    Amount of RAM in Mb you want to assign. Default 4096"
  echo "-u --user   User you are installing this for. Default $(whoami)"
  echo "-s --size   Amount of disk space to allocate to VM. Default 20G"
  echo "-h --help   This help message"
  echo "-----"
  echo "Ex. pirate-vm-iso -i /home/OS/os.iso -n GenericOS"

}

while [ "$1" != "" ] 
do
  case $1 in 
    -i | --iso)
      shift
      ISO="$1"
      ;;
    -n | --name)
      shift
      NAME="$1"
      ;;
    -r | --ram)
      shift
      RAM="$1"
      ;;
    -u | --user)
      shift
      USR="$1"
      ;;
    -s | --size)
      shift
      SIZE="$1"
      ;;
    -h | --help)
      helpMenu
      exit 1
      ;;
    *)
      helpMenu
      exit 1
      ;;
  esac
  shift
done

if [[ "$USR" == "" ]]; then
  USR="$(whoami)"
fi

if [[ "$RAM" == "" ]]; then
  RAM=4096
fi

if [[ "$SIZE" == "" ]]; then
  SIZE=20G
fi

if [[ "$NAME" == "" ]]; then
  echo "You must enter a name for your VM"
  echo "-"
  helpMenu
  exit 1
fi

vmLst=( $(ls /home/"$USR"/vm/) )
ctr=0
for i in "${vmLst[@]}"
do
  if [[ $NAME == ${vmLst[$ctr]} ]]; then
    echo "Error: VM Name already in use"
    echo "Do you want to remove the the VM directory and all files associated?"
    echo "*WARNING* This will permenantly delete any data stored within the directory /home/$USR/vm/$NAME"
    echo -n "Proceed (y/N): "
    read ans
      if [[ "$ans" == "y" ]] || [[ "$ans" == "Y" ]]; then 
        rm -rf /home/"$USR"/vm/$NAME
        break
      else
        echo "Quiting VM Build..."
        sleep 2
        exit 1
      fi
      
  fi
  ctr=$(( $ctr + 1 ))
done

if [[ "$ISO" == "" ]]; then
  echo "You must enter a path to a .iso" 
  echo "-"
  helpMenu
  exit 1
fi

if [[ ! -f "$ISO" ]]; then
  echo "$ISO does not exist"
  echo "-"
  helpMenu
  exit 1
fi

mkdir /home/"$USR"/vm/"$NAME"
cp $ISO /home/"$USR"/vm/"$NAME"/cd.iso
qemu-img create /home/"$USR"/vm/"$NAME"/hdd.img "$SIZE"

bwrap \
  --ro-bind / / \
  --dev-bind /dev/ /dev/ \
  --tmpfs /var/log/ \
  --tmpfs /usr/src \
  --tmpfs /lib/modules \
  --tmpfs /usr/lib/modules \
  --tmpfs /run/ \
  --tmpfs /boot/ \
  --tmpfs /mnt/ \
  --tmpfs /tmp/ \
  --tmpfs /root/ \
  --tmpfs /home/ \
  --tmpfs /tmp/ \
  --bind /home/"$USR"/vm/"$NAME" /home/user/vm \
  --dir /run/user/"$(id -u)" \
  --ro-bind /run/user/"$(id -u)"/wayland-1 /run/user/"$(id -u)"/wayland-1  \
  --unshare-all \
  --share-net \
  --die-with-parent \
  --new-session \
  --cap-drop all \
  /usr/bin/qemu-system-x86_64 -enable-kvm -usb -drive file=/home/"$USR"/vm/hdd.img,format=raw \
  -monitor stdio -vga std -m "$RAM" -cdrom /home/"$USR"/vm/cd.iso -boot d &>/home/"$USR"/.vmLogs/build.txt &
  
#   Maybe add this back in? can only be run with sudo so... maybe not. 
#     --cap-drop all \
#   --cap-add CAP_DAC_OVERRIDE \
