# About PlagueOS
PlagueOS is not associated with the TOR project or Whonix. 
This is not developed by an organization; this is a passion project created and maintained by a few Linux enthusiasts. 
This OS is not resistant to all forms of exploitation; there are shortcomings inherent to the modern desktop environment that we are attempting to mitigate. 
The OS is designed to be a hypervisor (host) with minimal attack surface that runs security-centric virtual machines (guests) from `virt-manager` 
Whonix and Kicksecure can be imported and validated during the installation script. Any other guests will be a manual endeavor. 

# Proper Usage
There are two users by default: `admin` and `user`
All updates, upgrades, and package installations (perhaps KeepassXC or ZuluCrypt) must be installed by the `admin` account.
All daily activities are to be conducted by the unprivileged `user` account. The `user` account has permissions to access `virt-manager` and the related guests.

## Mounting Devices
`USBGuard` is present on the device to prevent auto-mounting of USBs as these are a commonly known attack vector. To mount a USB device, you must run `usb-guard list-devices` and `usbguard allow-device <number>` from the `admin` account. To permanently save the rule, append a `-p` to the `usbguard allow-device <number>` command. 

## Traffic Routing
By default, the packages `openvpn` and `wireguard` are not present on the host. If a VPN is desired, these packages or custom wrappers from the provider must be installed. 
TOR can be configured during install to be leveraged for all package updates and installations. Do note that all traffic is not routed through TOR, and software-based routing solutions are not impervious to leakage. 

# Limitations
This OS cannot mitigate the problem of user error. If you are acting outside of the bounds of (proper usage)[#proper-usage], this is outside the scope of our mitigations. 

# Baby Steps
Do note that this project is still in its infancy, and bugs across different classes of hardware are likely to occur. We will help with bugs the best that we can, however we cannot investigate an error that we cannot reproduce or visualize. We appreciate any assistance with bug reporting.

# Development
All contributions to the PlagueOS project are welcome. This could include actively testing exploitation mechanisms against the OS from VM escapes to privilege escalation from the user account, pushing or providing steps to remove a bug, steps to include to implement one of the listed feature requests, or porting over software to Void Linux such as OpenSnitch that can stand to increase the overall security of PlagueOS. 